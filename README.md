# API Documentation

This readme documents all the different endpoints of the API.

## User Endpoints

### Register a User

METHOD
```
POST
```

URL
```
logger/register/
```

REQUEST BODY 
```json
{
  "username": "<str>",
  "password": "<password>"
}

```

### Login a user

METHOD
```
POST
```

URL
```
logger/rest-auth/login/
```

REQUEST BODY
```json
{
  "username": "<str>",
  "password": "<str>"
}
```

### Logout a user

This endpoint logs out a user based on a given token

METHOD
```
POST
```

URL
```
logger/rest-auth/logout/
```

REQUEST HEADERS
```
Authorization: Token <The token received from the login endpoint>
```

## Time Log Endpoints

### Get List of Time Logs from a User

METHOD
```
GET
```

URL
```
logger/api/time-logs/
```

POSSIBLE QUERY PARAMETERS
```
date = date string in the format %Y-%m-%d
```

`date` will filter the time logs to a specific date.
If no date is provided, the endpoint will return the
time logs intended for the current day.

REQUEST HEADERS
```
Authorization: Token <Login token>
```

### Query a specific time log

METHOD
```
GET
```

URL
```
logger/api/time-logs/:id
```

URL PARAMETERS
```
id = integer ID of the time log being queried
```

### Create a new Time Log

METHOD
```
POST
```

URL
```
logger/api/time-logs/
```

REQUEST HEADERS
```
Authorization: Token <token received from Login>
```

REQUEST BODY
```json
{
  "duration": "<float>",
  "project": "<int>",
  "remarks": "<str>",
  "log_date": "<date>"
}
```

Additional details about the request parameters:
```
"project": the integer primary key of the project,
"log_date": Date is formatted as %Y-%m-%d,
```

### Update a time log

METHOD
```
PUT
```

URL
```
logger/api/time-logs/:id
```

REQUEST HEADERS
```
Authorization: Token <token received from login>
```

REQUEST BODY
```json
{
  "duration": "<float>",
  "project": "<int>",
  "remarks": "<str>",
  "log_date": "<date>"
}
```

## Project Endpoints

### Add a Project

METHOD
```
POST
```

URL
```
logger/api/projects/
```

REQUEST HEADERS
```
Authorization: Token <token received from login>
```

Note: You must use a token intended for an admin user for this to work!

REQUEST BODY
```json
{
  "name": "<str>"
}
```

### Remove a Project

METHOD
```
DELETE
```

URL
```
logger/api/projects/:id
```

URL PARAMETERS
```
id = the integer ID of the project to be deleted
```

Note: You must use a token intended for an admin user for this to work!

### Assign users to a project

This method assigns a user to a specific project

METHOD
```
POST
```

URL
```
logger/api/projects/:project_id/user/:user_id
```

URL PARAMETERS
```
project_id = The ID of the project to assign the user to
user_id = The id of the user to assign to the project
```
