from django.shortcuts import redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from .models import TimeLog, Project
import datetime
from django.urls import reverse
from .forms import TimeLogFormSet
from django.views.generic import FormView
from django.http import HttpResponseBadRequest
from django.contrib.auth.models import User
from django.utils import timezone

# REST API Module Imports
from rest_framework import generics, viewsets, permissions
from .serializers import TimeLogSerializer, ProjectSerializer, UserSerializer
from .permissions import IsOwner, IsAssignedToProject
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response


# Create your views here.
# REST API Views
class TimeLogViewSet(viewsets.ModelViewSet):
    """
    Provides endpoints to create, read, update, and delete
    time logs.
    """
    serializer_class = TimeLogSerializer
    permission_classes = (permissions.IsAuthenticated, IsOwner, IsAssignedToProject)

    def perform_create(self, serializer):
        serializer.validated_data['user'] = self.request.user
        return super(TimeLogViewSet, self).perform_create(serializer)

    def get_queryset(self):
        queryset = TimeLog.objects.filter(user__exact=self.request.user)
        query_date = self.request.query_params.get('date', None)
        if query_date is not None:
            queryset = queryset.filter(log_date__exact=query_date)
        else:
            queryset = queryset.filter(log_date__exact=timezone.now().date())
        return queryset


class UserCreate(generics.CreateAPIView):
    """
    Endpoint that registers users into the system
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer


class ProjectViewSet(viewsets.ModelViewSet):
    """
    Project view set which automatically has all CRUD operations
    as well as indexing, but only for staff members.
    """
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer
    permission_classes = (permissions.IsAdminUser, permissions.DjangoModelPermissions)


@api_view(['POST', 'DELETE'])
@permission_classes((permissions.IsAdminUser, ))
def assign_user(request, project_pk, user_pk):
    """
    An admin-only endpoint that allows admin to assign users
    to a project or to remove users from a project.
    """
    project = get_object_or_404(Project, pk=project_pk)
    user = get_object_or_404(User, pk=user_pk)

    if request.method == 'POST':
        project.assigned_users.add(user)
        return Response('User {} added to project {}.'.format(user, project))

    elif request.method == 'DELETE':
        project.assigned_users.remove(user)
        return Response('User {} removed from project {}.'.format(user, project))


@method_decorator(login_required, name='dispatch')
class DashboardView(FormView):
    form_class = TimeLogFormSet
    template_name = 'index.html'
    success_url = '/'

    def dispatch(self, request, *args, **kwargs):
        """Used to validate the URL Parameters"""
        try:
            day = int(self.kwargs['day'])
            month = int(self.kwargs['month'])
            year = int(self.kwargs['year'])
        except TypeError:
            return HttpResponseBadRequest('Invalid year, month, or day in URL parameters')
        if month < 1 or month > 12:
            return HttpResponseBadRequest('Invalid month')
        if day < 1 or day > 31:
            return HttpResponseBadRequest('Invalid day')
        self.kwargs['day'] = day
        self.kwargs['month'] = month
        self.kwargs['year'] = year
        return super(DashboardView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(DashboardView, self).get_context_data(**kwargs)
        year = self.kwargs['year']
        month = self.kwargs['month']
        day = self.kwargs['day']
        user = self.request.user
        log_date = datetime.date(year, month, day)
        context['logs'] = TimeLog.objects.get_logs_of_day(log_date, user)
        context['projects'] = Project.objects.all()
        context['total_for_day'] = TimeLog.objects.get_total_for_day(log_date, user)
        context['total_for_month'] = TimeLog.objects.get_total_for_month(log_date, user)
        context['total_for_week'] = TimeLog.objects.get_total_for_week(log_date, user)
        return context

    def form_valid(self, formset):
        for data in formset.cleaned_data:
            if data:
                log_date = datetime.datetime(self.kwargs['year'], self.kwargs['month'], self.kwargs['day']).date()
                input_duration = data['duration']
                input_project = data['project']
                input_remarks = data['remarks']
                t = TimeLog(duration=input_duration,
                            project=input_project,
                            remarks=input_remarks,
                            log_date=log_date,
                            user=self.request.user)
                t.save()
        return redirect(reverse('logger_app:day_detail', kwargs={'year': self.kwargs['year'],
                                                                 'month': self.kwargs['month'],
                                                                 'day': self.kwargs['day']}))


@login_required
def index(request):
    today = datetime.datetime.now()
    year = today.year
    month = today.month
    day = today.day
    path = '/'.join([str(x) for x in [year, month, day]]) + '/'
    return redirect(path)