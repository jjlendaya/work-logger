from rest_framework import permissions
from rest_framework.permissions import exceptions
from .models import Project


class IsOwner(permissions.BasePermission):
    """
    Custom permission to allow only users to modify and read
    a specific object instance.
    """
    def has_object_permission(self, request, view, obj):
        return obj.user == request.user


class IsAssignedToProject(permissions.BasePermission):
    """
    Custom permission that checks whether a user owns a particular
    project that a time log is being placed into
    """
    def has_permission(self, request, view):
        if request.method == 'POST':
            project_filter = Project.objects.filter(assigned_users__exact=request.user, pk=request.data['project'])
            if project_filter.count() > 0:
                return True
            else:
                raise exceptions.PermissionDenied('You do not belong to this project.')
        else:
            return True
