from rest_framework import serializers
from .models import TimeLog, Project
from django.contrib.auth.models import User


class TimeLogSerializer(serializers.ModelSerializer):
    class Meta:
        model = TimeLog
        fields = ('id', 'duration', 'project', 'remarks', 'log_date', 'publish_date')
        extra_kwargs = {
            'id': {'read_only': True},
            'user': {'read_only': True},
            'publish_date': {'read_only': True},
            'project': {'required': True}
        }


class ProjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Project
        fields = ('id', 'name')
        extra_kwargs = {
            'id': {'read_only': True}
        }


class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True)

    class Meta:
        model = User
        fields = ('id', 'username', 'password')

    def create(self, validated_data):
        user = User.objects.create(
            username=validated_data['username']
        )
        user.set_password(validated_data['password'])
        user.save()
        return user
