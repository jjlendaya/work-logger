from django.db import models
import datetime


class TimeLogManager(models.Manager):

    def get_logs_of_day(self, date, user):
        """
        Convenience filter for getting all logs for a certain day from a user
        :param date: The date to be filtered by
        :param user: The user to be filtered by
        :return: All logs for a day
        """
        return super(TimeLogManager, self).get_queryset().filter(log_date__exact=date, user__exact=user)

    def get_total_for_day(self, date, user):
        """
        Convenience filter for getting the total hours of logs for a specific day
        :param date: The date to filter by
        :param user: The user to filter by
        :return: The total hours spent working on a given day
        """
        total_logged_hours = 0
        for log in self.get_queryset().filter(log_date__exact=date, user__exact=user):
            total_logged_hours += log.duration
        return total_logged_hours

    def get_total_for_month(self, date, user):
        """
        Convenience function for getting the total of logs for a certain month
        :param date: The date to use for filtering
        :param user: The user to use for filtering
        :return: The total hours logged
        """
        total_logged_hours = 0
        month = date.month
        for log in self.get_queryset().filter(log_date__month=month, user__exact=user):
            total_logged_hours += log.duration
        return total_logged_hours

    def get_total_for_week(self, date, user):
        """
        Convenience function for getting the total of logs for the same week as date
        Note that we treat the days as starting from Sunday to Saturday of the week
        :param date: The date to use for filtering
        :param user: The user to use for filtering
        :return: The total hours logged for the week
        """
        total_logged_hours = 0
        week_start = date - datetime.timedelta(days=date.weekday() + 1)
        week_end = week_start + datetime.timedelta(days=7)
        for log in self.get_queryset().filter(log_date__range=[week_start, week_end], user__exact=user):
            total_logged_hours += log.duration
        return total_logged_hours
