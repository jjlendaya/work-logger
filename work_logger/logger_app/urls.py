from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns
from rest_framework.routers import DefaultRouter
from rest_framework.authtoken.views import obtain_auth_token

from . import views


router = DefaultRouter()
router.register(r'projects', views.ProjectViewSet)
router.register(r'time_logs', views.TimeLogViewSet, 'time_logs')

app_name = 'logger_app'
urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^(?P<year>[0-9]+)/(?P<month>[0-9]+)/(?P<day>[0-9]+)/$', views.DashboardView.as_view(), name='day_detail'),
    url(r'^api/', include(router.urls)),
    url(r'^api/projects/(?P<project_pk>[0-9]+)/user/(?P<user_pk>[0-9]+)$', views.assign_user),
    url(r'^register/$', views.UserCreate.as_view()),
    url(r'^rest-auth/', include('rest_auth.urls'))
]