from django import forms
from django.forms import formset_factory
from .models import Project, TimeLog


class TimeLogForm(forms.ModelForm):
    duration = forms.FloatField(
        label='Duration of Hours',
        required=True,
        widget=forms.TextInput(attrs={
            'class': 'form-control',
            'placeholder': '(e.g. 1.5)'
        })
    )
    project = forms.ModelChoiceField(
        queryset=Project.objects.all().order_by('name'),
        required=True,
        widget=forms.Select(attrs={
            'class': 'form-control'
        })
    )
    remarks = forms.CharField(
        max_length=200,
        required=True,
        widget=forms.TextInput(attrs={
            'class': 'form-control'
        })
    )

    class Meta:
        model = TimeLog
        fields = ('duration', 'project', 'remarks')


TimeLogFormSet = formset_factory(TimeLogForm, extra=3)
