from django.apps import AppConfig


class LoggerAppConfig(AppConfig):
    name = 'logger_app'
