from django.db import models
from django.contrib.auth.models import User
from .managers import TimeLogManager


# Create your models here.
class TimeLog(models.Model):
    """
    Models hours worked on a specific project with remarks
    """
    duration = models.FloatField()
    project = models.ForeignKey('Project', on_delete=models.SET_NULL, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    remarks = models.CharField(max_length=200)
    log_date = models.DateField()
    publish_date = models.DateField(auto_now_add=True)
    objects = TimeLogManager()

    class Meta:
        ordering = ('-log_date', 'project', 'duration')

    def __str__(self):
        if self.project is not None:
            return "{} - {}: {}h".format(self.project.name, self.log_date, self.duration)
        else:
            return "{}: {}h".format(self.log_date, self.duration)

    def is_late_log(self):
        return self.publish_date > self.log_date


class Project(models.Model):
    """
    Models Projects in the system that receives work logs
    """
    name = models.CharField(max_length=200)
    assigned_users = models.ManyToManyField(User)

    class Meta:
        ordering = ('name', )

    def __str__(self):
        return self.name

    def display_total_logged_hours(self):
        total_logged_hours = 0
        for time_log in self.timelog_set.all():
            total_logged_hours += time_log.duration
        return total_logged_hours
    display_total_logged_hours.short_description = 'Total logged hours'
