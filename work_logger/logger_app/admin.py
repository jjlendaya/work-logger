from django.contrib import admin
from .models import TimeLog, Project


# Register your models here.
@admin.register(TimeLog)
class TimeLogAdmin(admin.ModelAdmin):
    model = TimeLog


@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    model = Project
    list_display = ('name', 'display_total_logged_hours')
